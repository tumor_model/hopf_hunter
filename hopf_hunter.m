% The goal of this program is to find the values of c for which there is a
% Hopf bifurcation. These occur at points where out of the three
% eigenvalues, two are imaginary, one is real, and the real one is zero.
% In addtion, the derivative at this point should not be zero.


% Store the fixed parameters in a struct so we don't have to define them
% multiple times
tumor_p.mu_2 = 0.03;
tumor_p.p_1 = 0.1245;
tumor_p.g_1 = 20000000;
tumor_p.g_2 = 100000;
tumor_p.r_2 = 0.18;
tumor_p.b = 0.000000001;
tumor_p.a = 1;
tumor_p.mu_3 = 10;
tumor_p.p_2 = 5;
tumor_p.g_3 = 1000;

% s_1 and s_2 represent treatments, at this stage we are not looking at
% any treatments so these are both zeros
tumor_p.s_1 = 0;
tumor_p.s_2 = 0;

c_values = 0:0.001:0.05; % Range from paper.
real_eigenvalues = nan(1, length(c_values));
real_c_values = nan(1, length(c_values));
real_counter = 1;
%Added in imaginary eigenvalues Kenn 3/26/17
imag_eigenvalues = nan(1, length(c_values));
imag_counter = 1;


for i = 1:length(c_values)
    
    % Get the c value, update it in the struct
    c = c_values(i);
    tumor_p.c = c;
    
    % Get the struct with all equilibria
    eq_struct = get_equilibrium(tumor_p);
    
    % Get rid of negative/imaginary equilibrium points
    eq_matrix = prune_equilibria(eq_struct);
    
    % Get the number of equilibria
    [num_equilibria, ~] = size(eq_matrix);
    
    eigen_array = {};
    
    for j = 1:num_equilibria
        equilibrium = eq_matrix(j, :);
        eigen_column = get_eigen(equilibrium, tumor_p);
        
        % Check if two are imaginary and one is real
        real_indices = zeros(1, length(eigen_column));
        for k = 1:length(eigen_column)
            real_indices(k) = isreal(eigen_column(k));
        end
        % If there are two imaginary and one real eigenvalues, then this is
        % one of the values we are interested in. Save it in the vector
        if sum(real_indices) == 1
            % Find which element is the real one
            real_element = find(real_indices);
            real_eigenvalues(real_counter) = eigen_column(real_element);
            % Save the associated c value
            real_c_values(real_counter) = tumor_p.c;
            real_counter = real_counter + 1;
            %If there is only one real eigenvalue, then we want to check to
            %see if there are two complex eigenvalues. 
            %First I want to create an array of complex indices. Kenny
            %3/26/17
            complex_indices = ones(1, length(real_indices));
            for k = 1:length(real_indices)
                complex_indices(k) = complex_indices(k) - real_indices(k);
            end
            if sum(complex_indices) == 2
                imag_element = find(complex_indices);
                imag_eigenvalues(imag_counter) = real(eigen_column(imag_element(1)));
                imag_counter = imag_counter + 1;
            end
        end
  
            
    end
end
%I don't think this plot is what we are looking for. Sorry Vera Kenny
%3/27/17
%plot(real_c_values, real_eigenvalues)
%title('Looking for Hopf Bifurcation');
%xlabel('Agenticity (c)');
%ylabel('Value of Real Eigenvalue');
%Plot the imag_eigenvalues vs. c Kenny 3/26/17
imaginary_figure = figure; 
plot(real_c_values, imag_eigenvalues)
title('Real Part of Complex Eigenvalues vs. Antigenicity (c)');
xlabel('Antigenicity');
ylabel('Value of Real Part of Complex Eigenvalues');

